﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{
    [SerializeField] GameObject arrow;
    private float rotationSpeed = 100f;
    private float moveSpeed = 20f;
    private bool isMoving = false;
    private int clicked = 0;
    private float clickedTime = 0f;

    void FixedUpdate()
    {
        LookAtMouse();

        if (Input.GetButton("Fire1") && isMoving == false)      //when player holds arrow is getting bigger and more force is added
        {
            clicked++;
            clickedTime = Time.time;
            Debug.Log(Time.time - clickedTime);
            arrow.transform.localScale += new Vector3(0.01f, 0f, 0.01f);
        }

        if (clicked > 1 && Time.time - clickedTime > 0.15f)
        {
            arrow.SetActive(false);
            gameObject.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward) * moveSpeed * clicked);
            isMoving = true;
            clicked = 0;
        }
    }

    private void Update()
    {
        if (arrow.activeSelf == false)
        {
            StartCoroutine(CheckMoving());

            if (isMoving == false)
            {
                arrow.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                arrow.SetActive(true);
                isMoving = false;
            }
        }
    }

    private void LookAtMouse()
    {
        var playerPlane = new Plane(Vector3.up, transform.position);

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float hitdist = 0f;
        if (playerPlane.Raycast(ray, out hitdist))
        {
            var targetPoint = ray.GetPoint(hitdist);
            var targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, moveSpeed * Time.deltaTime);
        }
    }

    private IEnumerator CheckMoving()
    {
        Vector3 startPos = transform.position;
        yield return new WaitForSeconds(0.5f);
        Vector3 finalPos = transform.position;
        if ((startPos.x - finalPos.x > 0.5f) || (startPos.y - finalPos.y > 0.5f) || (startPos.z - finalPos.z > 0.5f)) { isMoving = true; }
        else {isMoving = false; }
    }
}

